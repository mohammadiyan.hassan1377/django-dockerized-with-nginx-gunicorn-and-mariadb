# Django app 
## Dockerized with Nginx, Gunicorn, MariaDB
### Project Detail

You can find all technologies we used in our project into these files:
* Version: 1.0.0
* Back-End: Django
* Language: Python
* OS: Ubuntu 22.04
* DevOps Pipline
  * Docker
  * Docker Compose
  * Nginx alpine
  * Language: Python alpine
  * Framework: Django
  * OS: Ubuntu
  * CVS: Git / GitLab
  * Database: mariaDB

## Start building the project
### Clone project from gitlab
 cd main directory and following commands to get the build.
``` sh
$ docker-compose up --build
```

 Also run the following command to shut down the project.
 ``` sh
$ docker-compose down
```